let arr = [1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

function binarySearch(arr){
  
  if(arr.length === 2){
    if(arr[0]===arr[1]){
      return arr[1]
    }
    return false
  }
  
  let halfLength = Math.ceil(arr.length/2);
  
  
  let leftArr = arr.slice(0,halfLength),
      rightArr = arr.slice(-halfLength);

  
   let resLeft = binarySearch(leftArr)
   
   if(resLeft){
     return resLeft
   }
    let resRight = binarySearch(rightArr);
    if(resRight){
       return resRight
     }
  
   let newArr = arr.slice(0, -1)
   return binarySearch(newArr)
   

}

console.log(binarySearch(arr))